import { Button, Box, Stack } from '@mui/material';
import { Component, ReactElement } from 'react';
import { Calendar } from '../model/Calendar';
import IconButton from '@mui/material/IconButton';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import CalendarForm from './CalendarForm';

interface SettingsProps {
  calendars: Array<Calendar>;
  onValidate: (calendars: Array<Calendar>) => void;
}

interface SettingsState {
  calendars: Array<Calendar>;
}

export class Settings extends Component<SettingsProps, SettingsState> {
  constructor(props: any) {
    super(props);
    let calendars = this.props.calendars;
    if (calendars.length === 0) calendars.push(new Calendar('', ''));
    this.state = { calendars };
  }

  addCalendar() {
    let calendar = new Calendar('', '');
    this.setState({ calendars: [...this.state.calendars, calendar] });
  }

  removeCalendar(indexToRemove: number) {
    this.setState({
      calendars: this.state.calendars.filter(
        (v, index) => index !== indexToRemove
      ),
    });
  }

  updateCalendar(index: number, newCalendar: Calendar) {
    this.setState({
      calendars: this.state.calendars.map((oldCalendar, currentIndex) =>
        currentIndex === index ? newCalendar : oldCalendar
      ),
    });
  }

  onValidate() {
    let filteredCalendars = this.state.calendars.filter(
      (cal) => cal.name !== '' && cal.url !== ''
    );
    console.log(filteredCalendars);
    if (filteredCalendars.length > 0) {
      this.props.onValidate(filteredCalendars);
    }
  }

  render() {
    let calendarForms: ReactElement[] = [];
    for (let i = 0; i < this.state.calendars.length; i++) {
      calendarForms.push(
        <CalendarForm
          key={i}
          calendar={this.state.calendars[i]}
          onChange={(calendar) => this.updateCalendar(i, calendar)}
          onDelete={() => this.removeCalendar(i)}
        />
      );
    }

    return (
      <Box
        id={'view'}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          maxWidth: '50rem',
          margin: 'auto',
        }}
      >
        <h3> Calendriers </h3>
        <p className="url-label">
          Url du calendrier en ical (
          <a
            href={
              'https://gitlab.com/nilsponsard/oh-my-ade/-/wikis/Comment-obtenir-l%E2%80%99url-du-calendrier'
            }
          >
            comment l'obtenir
          </a>
          ) :
        </p>
        <Stack
          direction="row"
          spacing={2}
          sx={{ width: '100%', textAlign: 'left' }}
        >
          <Button variant="contained" onClick={(event) => this.onValidate()}>
            Sauvegarder
          </Button>
          <Button
            variant="outlined"
            onClick={(event) =>
              this.setState((state) => ({
                calendars: this.props.calendars,
              }))
            }
          >
            Annuler les changements
          </Button>
        </Stack>

        <Stack spacing={2} sx={{ marginTop: 2 }}>
          {calendarForms}
        </Stack>

        <IconButton
          onClick={(event) => this.addCalendar()}
          color="primary"
          sx={{
            margin: 'auto',
            width: '4rem',
            height: '4rem',
            marginTop: '2rem',
            fontSize: '2rem',
          }}
          size="large"
        >
          <AddCircleIcon fontSize="large" />
        </IconButton>
      </Box>
    );
  }
}
